import { Component, Input } from '@angular/core';
import { ComprasService } from '../service/compras.service';
import { ICompras } from '../compras';
import jsPDF from 'jspdf';

@Component({
  selector: 'app-exito',
  templateUrl: './exito.component.html',
  styleUrls: ['./exito.component.css']
})
export class ExitoComponent {
  

  constructor(private compraService: ComprasService){}

  compras: ICompras[];

  ngOnInit(){
    this.compraService.getCompraMax().subscribe((res:any[])=>{
      this.compras = res, 
      console.log(this.compras);
    },
    err => console.log(err)
    )
  }

  descargar(){
    const doc = new jsPDF();
  
  
    doc.text("Decibelios APP", 85, 10);
    doc.text("Tickets para el evento: "+this.compras[0].NombreEvento, 10, 20);
    doc.text("Entrada", 10, 40);
    doc.text("Cantidad", 70, 40);
    doc.text("Total", 150, 40);
    let y = 50;
    for(let i = 0; i < this.compras.length; i++){
      doc.text(this.compras[i].nombreTicket, 10, y);
      doc.text(this.compras[i].cantidad.toString(), 70, y);
      let totaltmp = this.compras[i].precioTicket*this.compras[i].cantidad;
      doc.text(totaltmp.toString(), 150, y);
      y+=10;
    }
    doc.setLineWidth(0.5);
    doc.line(10, y-7, 170, y-7);
    doc.text(this.compras[0].Total.toString(), 150, y);
    doc.addImage("../../assets/frame.png", "png", 15, y+10, 180, 180);

    doc.save('tickets.pdf');
    console.log("EXTIOOOOOO")
  }

}
