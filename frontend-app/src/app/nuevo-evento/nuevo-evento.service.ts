import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
  })
export class NuevoEventoService{
    inNuevo: boolean = false;

    leaveNuevoEvento(){
        this.inNuevo = false;
    }
}