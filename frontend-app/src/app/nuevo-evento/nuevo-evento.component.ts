import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EventosService } from '../service/evento/eventos.service';
import { NuevoEventoService } from './nuevo-evento.service';

@Component({
  selector: 'app-nuevo-evento',
  templateUrl: './nuevo-evento.component.html',
  styleUrls: ['./nuevo-evento.component.css']
})
export class NuevoEventoComponent implements OnInit {
  fG: FormGroup;

  constructor(private fb: FormBuilder, private eventoService: EventosService, private nuevoService: NuevoEventoService) {}

  ngOnInit(): void {
    this.fG = this.fb.group({
      name: ['', Validators.required],
      file: [''],
      file2: [''],
      desc: ['', Validators.required],
      price: [null, Validators.required],
      dateI: [null, Validators.required],
      dateF: [null, Validators.required],
      timeI: [null, Validators.required],
      timeF: [null, Validators.required],
      place: ['', Validators.required],
      cat: ['', Validators.required],
      decs2: ['', Validators.required]
    });
  }

  submitForm() {
    if (this.fG.valid) {
      this.eventoService.saveEvento(this.fG.value).subscribe(
        response => {
          console.log('Event data saved successfully:', response);
          // Handle success, if needed
          this.nuevoService.leaveNuevoEvento();
        },
        error => {
          console.error('Error saving event data:', error);
          // Handle error, if needed
        }
      );
    }
  }
}
