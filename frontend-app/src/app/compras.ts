export interface ICompras{
    Compra_id:number,
    Ticked_id: number,
    Usuario_id: number,
    Evento_id: number,
    cantidad:number,
    nombreTicket: string,
    precioTicket: number,
    Pdf: string,
    Total: number,
    FechaCompra: Date,
    FechaEvento:Date,
    NombreEvento:string
}