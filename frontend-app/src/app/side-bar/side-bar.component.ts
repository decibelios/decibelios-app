import { Component } from '@angular/core';
import { LoginService } from '../service/login/login.service';
import { EventosService } from '../service/evento/eventos.service';
import { CrudService } from '../crud/crud.service';
import { NuevoEventoService } from '../nuevo-evento/nuevo-evento.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent {
  evento = false;
  historial = false;
  eventoNuevo = false;
  compras = false;

  constructor(public logService: LoginService, private eventoService: EventosService, private crudService: CrudService, private nuevoService: NuevoEventoService) {
    console.log('estoy en el crud');
    this.evento = crudService.inCrud;
    this.eventoNuevo = nuevoService.inNuevo;
  }
  Evento(){
    this.evento = true;
    this.historial = false;
    this.eventoNuevo = false;
    this.compras = false;
  }
  Historial(){
    this.historial = true;
    this.evento = false;
    this.eventoNuevo = false;
    this.compras = false;
  }
  nuevoEvento(){
    this.eventoNuevo = true;
    this.evento = false;
    this.historial = false;
    this.compras = false;
  }
  Compras(){
    this.compras = true;
    this.historial = false;
    this.evento = false;
    this.eventoNuevo = false;
  }
}
