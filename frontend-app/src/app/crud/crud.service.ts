import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
  })
export class CrudService{
    inCrud: boolean = false;

    leaveCrud(){
        this.inCrud = false;
    }
}