import { Component, OnInit } from '@angular/core';
import { EventosService } from '../service/evento/eventos.service';
import { IEventos } from '../eventos';
import { Router } from '@angular/router';
import { CrudService } from './crud.service';
import { Time } from '@angular/common';

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.css']
})
export class CrudComponent implements OnInit{
  
  eventos:IEventos[];
  idEvento: number;
  editarEvento: IEventos;
  name:string;
  file: string;
  file2: string;
  desc: string;
  price: number;
  dateI: Date;
  dateF: Date;
  timeI: Time;
  timeF: Time;
  place: string;
  cat: string;
  decs2: string;

    constructor(private eventoService: EventosService, private crudService: CrudService) {
      
    }
  ngOnInit(): void {
    this.eventoService.getProductos().subscribe((res:any[])=>{
        this.eventos = res,
        console.log(this.eventos);
        },
      err => console.log(err)
    )
  }
  updateEvent(id:any){
    this.idEvento = id;
    this.eventoService.getProductosId(id).subscribe((res:any)=>{
      this.editarEvento = res,
      console.log(this.editarEvento);
      },
      err => console.log(err)
    )
  }
  onSubmit(){
      let datos: any= {
        nombre: this.name,
        imagen: this.file,
        imagen_2: this.file2,
        descripcion: this.desc,
        precio: this.price,
        fecha_inicio: this.dateI,
        fecha_termino: this.dateF,
        hora_inicio: this.timeI,
        hora_termino: this.timeF,
        lugar: this.place,
        categoria: this.cat,
        descripcion2: this.decs2
      };
      this.eventoService.updateEvento(this.idEvento,datos).subscribe(
        response => {
          console.log('Datos actualizados exitosamente', response);
          this.crudService.leaveCrud();
        },
        error => {
          console.error('Error al editar Evento', error);
          // Handle error, if needed
        }
      );
  }
  deleteEvento(id:number){
    this.eventoService.deleteEvento(id).subscribe(
      response => {
        console.log('Datos Borrados exitosamente', response);
        this.crudService.leaveCrud();
      },
      error => {
        console.error('Error al borrar Evento', error);
        // Handle error, if needed
      }
    );
  }
}
