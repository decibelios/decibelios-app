import { Component } from '@angular/core';
import { IEventos } from '../eventos';
import { EventosService } from '../service/evento/eventos.service';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.css']
})
export class HistorialComponent {
  eventos:IEventos[];
  constructor(private eventoService: EventosService) {
    
  }
ngOnInit(): void {
  this.eventoService.getProductos().subscribe((res:any[])=>{
      this.eventos = res,
      console.log(this.eventos);
      },
    err => console.log(err)
  )
}
}
