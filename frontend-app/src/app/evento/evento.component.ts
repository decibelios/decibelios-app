import { Component, ElementRef, Inject, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { EventosService } from '../service/evento/eventos.service';
import { Subscription } from 'rxjs';
import { IEventos } from '../eventos';
import { CommonModule, DatePipe } from '@angular/common';
import { TicketsService } from '../service/tickets.service';
import { ITickets } from '../tickets';
import { MercadoPago } from '../mercado-pago';
import jsPDF from 'jspdf';
import { ICompras } from '../compras';
import { ComprasService } from '../service/compras.service';

@Component({
  selector: 'app-evento',
  templateUrl: './evento.component.html',
  styleUrls: ['./evento.component.css'],

})

export class EventoComponent {
  loginForm: FormGroup;
  id:any;

  evento:IEventos;
  tickets: ITickets[];
  tmp:number[];

  datepipe: DatePipe = new DatePipe('en-US')
  // Cambiar esta variable segun si esta logueado el usuario
  mostrarElemento: boolean = false;

  private routeSub: Subscription;
  totalCompra = 0;

  constructor(private compraService: ComprasService,private formBuilder: FormBuilder,private eventoService: EventosService,
    private ticketService: TicketsService,private route: ActivatedRoute){
    this.route.paramMap.subscribe(params => {
      this.ngOnInit();
  });
  }


  ngOnInit(){
    this.loginForm = this.formBuilder.group({
      cantidad: ['', [Validators.required]]
    });

    this.routeSub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    this.eventoService.getProductosId(this.id).subscribe((res:IEventos)=>{
      this.evento = res
      console.log(this.evento)
    },
      err => console.log(err)
    )


    this.ticketService.getTickets(this.id).subscribe((res:ITickets[])=>{
      this.tickets = res
      this.tmp = new Array(this.tickets.length)
      for(let i=0; i<this.tmp.length; i++){
        this.tmp[i] = 0;
      }
      console.log("ticket")
      console.log("Los tickets"+this.tickets);
      },
        err => console.log(err)
      )

      if (localStorage.getItem('user')) {
        this.mostrarElemento = true;
      }

      
  }

  submitForm() {
    if (this.loginForm.valid) {
      // Aquí puedes enviar los datos del formulario al backend para realizar el registro
    }
  }


  onChange(value: any,precio:any,ind:number) {
    this.tmp[ind] = value.target.value;
    this.totalCompra = 0
    for(let i=0; i<this.tmp.length; i++){
      this.totalCompra += this.tmp[i]*this.tickets[i].precio;
    }
  }

 
  pagar(){
    let idCom;
    this.compraService.getId().subscribe(
      data => {
        idCom = data[0].id+1;
      
    var prods: MercadoPago[] = [];
    let myDate = new Date();
    let compras: ICompras[] = [];
    for(let i = 0; i < this.tmp.length;i++){
        if(this.tmp[i] > 0){
          let item: MercadoPago = {
          title: this.tickets[i].nombre,
          unit_price: this.tickets[i].precio,
          quantity: this.tmp[i]
          }
          let compra: ICompras = {
            Compra_id:idCom,
            Evento_id:this.evento.id_evento,
            FechaCompra: myDate,
            FechaEvento:this.evento.fecha_inicio,
            NombreEvento:this.evento.nombre,
            cantidad:this.tmp[i],
            nombreTicket:this.tickets[i].nombre,
            precioTicket:this.tickets[i].precio,
            Pdf: "no se",
            Ticked_id: this.tickets[i].id,
            Total:this.totalCompra,
            Usuario_id: 2
          }
          compras.push(compra);
          prods.push(item);
        }
        console.log(compras);
    }
    
   
   this.eventoService.generarLinkMercadoPago(prods).subscribe(
    data => {
      window.open(data,"_blank");
    },
    error => {
      // Handle errors, if any
      console.error(error);
    }
  );

  this.compraService.postCompras(compras);
});
 } 


  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }
}
