import { Component,OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-olvide-pass',
  templateUrl: './olvide-pass.component.html',
  styleUrls: ['./olvide-pass.component.css']
})
export class OlvidePassComponent implements OnInit{

  olvideForm: FormGroup;
  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(){
    this.olvideForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }
  submitForm(){
    if (this.olvideForm.valid) {
      
    }
  }
}
