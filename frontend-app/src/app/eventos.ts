import { Time } from "@angular/common";

export interface IEventos{
    id_evento:number,
    nombre:string,
    imagen:string,
    imagen_2:string,
    descripcion:string,
    precio:number,
    fecha_inicio:Date,
    fecha_termino:Date,
    hora_Inicio:Time,
    hora_termino:Time,
    lugar:string,
    categoria:string,
    descripcion2:string,
}