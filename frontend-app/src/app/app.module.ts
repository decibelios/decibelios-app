import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './registro/registro.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { EventosComponent } from './eventos/eventos.component';
import { GaleriaComponent } from './galeria/galeria.component';
import { OlvidePassComponent } from './olvide-pass/olvide-pass.component';
import { FooterComponent } from './footer/footer.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { CrudComponent } from './crud/crud.component';
import { HistorialComponent } from './historial/historial.component';
import { EventoComponent } from './evento/evento.component';
import {HttpClientModule} from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { AuthGuard } from './login/auth.guard';
import { UserComponent } from './user/user.component';
import { LoginService } from './service/login/login.service';
import { NuevoEventoComponent } from './nuevo-evento/nuevo-evento.component';
import { NosotrosComponent } from './nosotros/nosotros.component';
import { ComprasComponent } from './compras/compras.component';
import { ExitoComponent } from './exito/exito.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegistroComponent,
    WelcomeComponent,
    EventosComponent,
    EventoComponent,
    GaleriaComponent,
    OlvidePassComponent,
    FooterComponent,
    SideBarComponent,
    CrudComponent,
    HistorialComponent,
    UserComponent,
    NuevoEventoComponent,
    NosotrosComponent,
    ComprasComponent,
    ExitoComponent,

  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path:'welcome', component: WelcomeComponent},
      {path:'evento', component: EventosComponent},
      {path:'galeria', component: GaleriaComponent},
      {path:'nosotros', component: NosotrosComponent},
      {path: 'login', component: LoginComponent, },
      {path: 'registro', component: RegistroComponent},
      {path:'forgot', component: OlvidePassComponent},
      {path:'nuevoEvento', component:NuevoEventoComponent, outlet:'vistaAdmin'},
      {path: 'historial', component: HistorialComponent, outlet:'vistaAdmin'},
      {path: 'crud', component: CrudComponent, outlet:'vistaAdmin'},
      {path: 'user', component:UserComponent},
      {path:'admin', component: SideBarComponent, canActivate:[AuthGuard],},
      {path:'', redirectTo: 'welcome', pathMatch:'full'},
      {path:'evento-selec/:id', component: EventoComponent},
      {path:'exito', component: ExitoComponent},
    ])
  ],
  providers: [{provide: LOCALE_ID, useValue:'es-CL'}, LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
