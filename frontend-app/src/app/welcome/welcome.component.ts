import { Component } from '@angular/core';
import { EventosService } from '../service/evento/eventos.service';
import { IEventos } from '../eventos';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent {
  
  eventos:IEventos[];
  constructor(private eventoService: EventosService) {
    
  }


  ngOnInit(): void {
    this.eventoService.getProductos().subscribe((res:any[])=>{
            this.eventos = res,
            console.log(this.eventos);
      },
      err => console.log(err)
    )
  }
}
