import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RegisterService } from '../service/register/register.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],
  providers: [RegisterService]
})
export class RegistroComponent  implements OnInit {

  registroForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private registerService: RegisterService, private router: Router) { }

  ngOnInit() {
    this.registroForm = this.formBuilder.group({
      nombre: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      contraseña: ['', [Validators.required, Validators.minLength(6)]],
    });
  }
  submitForm() {
    if (this.registroForm.valid) {
      this.registerService.register(this.registroForm.value.nombre,this.registroForm.value.email,this.registroForm.value.contraseña).subscribe((res:any)=>{
        console.log(res)
        this.router.navigate(['login'])

      });
    }
  }


}
