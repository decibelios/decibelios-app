import { Component, NgZone, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../service/login/login.service';
import { IUsers } from '../users';
import { Location } from '@angular/common';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isVerticalNavActive = false;
  public userName: string;
 
  constructor(private router: Router, public logService: LoginService,private location: Location) {
    /*
    const userData = localStorage.getItem('user');
    if (userData){
      const user = JSON.parse(userData)[0];
      this.userName = user.nombre;
      
      
      console.log(this.userName)
    }
    */
  }
  
  ngOnInit() {
    this.isVerticalNavActive = this.router.url === '/vertical';
    this.router.events.subscribe(() => {
      this.isVerticalNavActive = this.router.url === '/vertical';
    });
    
    
    
  }
  
}
