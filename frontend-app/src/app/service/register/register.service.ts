import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient) { }

  register(nombre:any, email:any, password:any){
      let userRegister = {nombre:nombre, mail: email, password: password};
      return this.http.post('http://localhost:3000/user',userRegister);
  }
}
