import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { ICompras } from '../compras';

@Injectable({
  providedIn: 'root'
})
export class ComprasService {

  constructor(private http:HttpClient) { }

  getCompras():Observable<ICompras[]>{
      return this.http.get<ICompras[]>('http://localhost:3000/compras').pipe(map((res:any)=> res.data));
  }

  getCompraMax():Observable<ICompras[]>{
    return this.http.get<ICompras[]>('http://localhost:3000/compramax').pipe(map((res:any)=> res.data));
}

  getId(){
    return this.http.get<any>('http://localhost:3000/compraid').pipe(map((res:any)=> res.data));
  }

  postCompras(compras: ICompras[]){
    
    return this.http.post<any>('http://localhost:3000/compra/',compras).subscribe();
  }


}
