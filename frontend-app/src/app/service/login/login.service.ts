import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, map } from 'rxjs';
import { IUsers } from 'src/app/users';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public logueado: boolean = false;

  constructor(private http: HttpClient, private router:Router) { }

  public login(email:any, password:any){
    let userLogin = {mail: email, password: password};
    return this.http.post('http://localhost:3000/login', userLogin).pipe(map((res:any)=>{
      localStorage.setItem('token', res.token);
      localStorage.setItem('user', JSON.stringify(res.user));
      this.logueado = true;
      this.router.navigate(['admin'])
      console.log(this.logueado);
      return res;
    }))
  }
  public logout(){
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.logueado = false;
    this.router.navigate(['welcome'])
  }

  public loggedIn():boolean{
    return localStorage.getItem('token') !== null;
  }
}
