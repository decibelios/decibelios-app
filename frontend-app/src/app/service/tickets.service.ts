import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ITickets } from '../tickets';

@Injectable({
  providedIn: 'root'
})
export class TicketsService {

  constructor(private http:HttpClient) { }
  getTickets(id:any): Observable<ITickets[]>{
    return this.http.get<ITickets[]>('http://localhost:3000/tickets/'+id).pipe(map((res:any)=> res.data));
}
}
