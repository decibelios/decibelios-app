import { HttpClient } from '@angular/common/http';
import { identifierName } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { IEventos } from 'src/app/eventos';
import { MercadoPago } from 'src/app/mercado-pago';

@Injectable({
  providedIn: 'root'
})
export class EventosService {

  public eventos: IEventos[];

  constructor(private http:HttpClient) { }

  getProductos(): Observable<IEventos[]>{
      return this.http.get<IEventos[]>('http://localhost:3000/eventos').pipe(map((res:any)=> res.data));
  }

  deleteEvento(id:any){
    return this.http.delete('http://localhost:3000/evento/'+id);
  }

  saveEvento(evento:IEventos){
      return this.http.post<IEventos[]>('http://localhost:3000/evento', evento);
  }
  
  getProductosId(id :any): Observable<IEventos>{
    return this.http.get<IEventos>('http://localhost:3000/eventos/'+id).pipe(map((res:any)=> res.data));
}
  updateEvento(id:any,event:any){
    return this.http.put('http://localhost:3000/evento/'+id,event);
  }

  generarLinkMercadoPago(items:MercadoPago[]){
    console.log(items)
    return this.http.post<any>('http://localhost:3000/generar/',items).pipe(map((res:any)=> res.data));
  }

}
