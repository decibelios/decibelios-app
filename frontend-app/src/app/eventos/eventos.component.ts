import { Component, OnInit } from '@angular/core';
import { IEventos } from '../eventos';
import { EventosService } from '../service/evento/eventos.service';

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.css'],
  providers:[EventosService]
})
export class EventosComponent implements OnInit{
  
  eventos:IEventos[];
  constructor(private eventoService: EventosService) {
    
  }


  ngOnInit(): void {
    this.eventoService.getProductos().subscribe((res:any[])=>{
            this.eventos = res,
            console.log(this.eventos);
      },
      err => console.log(err)
    )
  }

}
