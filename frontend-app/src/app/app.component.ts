import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EventosService } from './service/evento/eventos.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Decibelios';
  isVerticalNavActive = false;
  constructor(private router: Router, private eventoService: EventosService){}

  ngOnInit(): void {
    this.isVerticalNavActive = this.router.url === '/admin';
    this.router.events.subscribe(() => {
      this.isVerticalNavActive = this.router.url === '/admin';
    });
  }
}
