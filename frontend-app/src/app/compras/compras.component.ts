import { Component, OnInit } from '@angular/core';
import { ComprasService } from '../service/compras.service';
import { ICompras } from '../compras';


@Component({
  selector: 'app-compras',
  templateUrl: './compras.component.html',
  styleUrls: ['./compras.component.css']
})
export class ComprasComponent implements OnInit {

  compras: ICompras[];  
  constructor(private conService:ComprasService) {
    
  }
  ngOnInit(): void {
    this.conService.getCompras().subscribe((res:any[])=>{
      this.compras = res, 
      console.log(this.compras);
    },
    err => console.log(err)
    )
  }
}
