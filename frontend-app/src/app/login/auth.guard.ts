import { Inject, Injectable, inject } from '@angular/core';
import {  CanActivateFn, Router} from '@angular/router';

import { LoginService } from '../service/login/login.service';
import { IUsers } from '../users';


export const AuthGuard: CanActivateFn = (route, state) =>{
  const router = inject(Router);
  const users: IUsers[]  = JSON.parse(localStorage.getItem('user') ?? '[]');
  if (users.find(user => user.rol === 'admin')) {
    return true;
  }else if (users.find(user => user.rol === 'user')) {
      router.navigate(['welcome']);
  }
  return false;
}