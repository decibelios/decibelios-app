var express = require('express');
const mysql = require('mysql');
const cors = require('cors');
const bodyParser = require('body-parser');
const bcrypt = require('bcryptjs');
var jwt= require('jsonwebtoken');
const fileUpload = require('express-fileupload');
const router = express.Router

var app = express();
app.use(fileUpload());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(cors());

//CONECTAR A DB
const mc = mysql.createConnection({
    host: '172.17.0.2',
    port: 3310,
    user: 'root',
    password: 'decibelios',
    database: 'angular'
});

mc.connect();


app.get('/eventos', function(req, res){
    mc.query('SELECT * FROM eventos', function(error, results, fields){
        if (error) throw error; 
        return res.send({
            error: false,
            data: results,
            mensaje: 'Lista de eventos.'
        });
    });
});
//SELECT MAX(Compra_id) FROM compras;
app.get('/compraid', function(req, res){
    mc.query('SELECT MAX(Compra_id) as id FROM compras', function(error, results, fields){
        if (error) throw error; 
        return res.send({
            error: false,
            data: results,
            mensaje: 'Lista de eventos.'
        });
    });
});
app.get('/compramax', function(req, res){
    mc.query('select * from compras where compra_id = (SELECT MAX(Compra_id) as id FROM compras)', function(error, results, fields){
        if (error) throw error; 
        return res.send({
            error: false,
            data: results,
            mensaje: 'Lista de eventos.'
        });
    });
});

app.get('/compras', function(req,res){
    mc.query('SELECT * FROM compras', function(error, results, fields){
        if (error) throw error;
        return res.send({
            error: false,
            data: results,
            mensaje: 'Lista de compra'
        })
    })
})



// MERCADO PAGO
// TOKEN: APP_USR-5416415321331505-073017-56e9cf7c663733cd18f7a6ad345c35a9-1436488459
// KEY APP_USR-2fb3ebb4-955d-403f-bf74-c8e4542901a2
// SDK de Mercado Pago
const mercadopago = require("mercadopago");
// Agrega credenciales
mercadopago.configure({
  access_token: "APP_USR-5416415321331505-073017-56e9cf7c663733cd18f7a6ad345c35a9-1436488459",
});

app.post('/generar', (req,res) =>{
    console.log("Entro a pago")
let preference = {
    back_urls:{
        success: 'http://localhost:4200/exito'
    },
    items: [],
  };
  
for(let i = 0; i < req.body.length; i++){
    preference.items.push({
        title: req.body[i].title,
        unit_price: Number(req.body[i].unit_price),
        quantity: Number(req.body[i].quantity),
      });
      //console.log( preference.items);
}

  mercadopago.preferences
    .create(preference)
    .then(function (response) {
      global.id = response.body.id;
      //console.log(response);
      res.send({data:response.body.init_point});
    })
    .catch(function (error) {
      console.log(error);
    });

    
});





//REGISTER
app.post('/user', function(req, res){
    let datosUser = {
        nombre : req.body.nombre,
        mail : req.body.mail,
        password : bcrypt.hashSync(req.body.password, 10),
        rol: 'user'
    };

    if (mc) {
        mc.query("INSERT INTO user SET ?", datosUser, function(error, result){
            if (error) {
                return res.status(400).json({
                    ok:false,
                    mensaje: 'Error al crear usuario',
                    errors: error
                });
            }
            else{
                res.status(201).json({
                    ok:true,
                    usuario: result
                });
            }
        });
    }
});



app.get('/', (req, res, next) =>{
    res.status(200).json({
        ok: true,
        mensaje: 'Peticion realizada correctamente'
    })
});

app.get('/user/:id', (req, res)=>{
    let id = req.params.id;
    if (mc) {
        console.log(id);
        mc.query("SELECT * FROM users WHERE id = ?", id, function(error, result){
            if (error) throw error; 
            return res.send({
                error: false,
                data: result,
                mensaje: 'user'
            });
        });
    }
});

//LOGIN
app.post('/login', (req, res)=>{
    var body = req.body;
    mc.query("SELECT * FROM user WHERE mail = ?", body.mail, function(error, results, fields){
        if (error) {
            return res.status(500).json({
                ok:false,
                mensaje: 'Error al buscar user',
                errors: error
            });
        }
        if (!results.length) {
            return res.status(400).json({
                ok:false,
                mensaje: 'Credenciales incorrentes - email',
                errors: error
            });
        }
        console.log(results);
        if (!bcrypt.compareSync(body.password, results[0].password)) {
            return res.status(400).json({
                ok:false,
                mensaje: 'Credemciales incorrectas - password', errors: error
            });
        }
        //TOKEN
        res.status(200).json({
            ok:true,
            user:results,
            id: results[0].id,
        });
    });
});





/*
app.use('/', (req, res, next)=>{
    let token = req.query.token;
    let SEED = 'its a seed';
    console.log(token);
    jwt.verify(token, SEED, (err,decoded)=>{
        if (err) {
            return res.status(401).json({
                ok:false,
                mensaje: 'Token incorrecto',
                errors: err
            });
        }
        req.usuario = decoded.usuario;
        next();
    });
});

*/


app.post('/evento', function(req, res){
    
    let datosEvento = {
        nombre : req.body.name,
        imagen : req.body.file,
        imagen_2: req.body.file2,
        descripcion: req.body.desc,
        precio: parseInt(req.body.price),
        fecha_inicio: req.body.dateI,
        fecha_termino: req.body.dateF,
        hora_Inicio: req.body.timeI,
        hora_termino: req.body.timeF,
        lugar: req.body.place,
        categoria:req.body.cat,
        descripcion2: req.body.decs2
    };
    if (mc) {
        mc.query("INSERT INTO eventos SET ?", datosEvento, function(error, result){
            if(error){
                res.status(500).json({"Mensaje":"Error"});
            }
            else{
                res.status(201).json({"Mensaje":"Insertado"});
            }
        });
    }
});

app.post('/compra', (req,res) =>{
    console.log("Entro a postcompra")
    let errors;
    for(let i = 0; i < req.body.length; i++){
        if (mc) {
            let datosCompra = {
                Compra_id:req.body[i].Compra_id,
                Ticket_id: req.body[i].Ticked_id,
                Usuario_id: req.body[i].Usuario_id,
                Evento_id:req.body[i].Evento_id,
                cantidad:req.body[i].cantidad,
                nombreTicket:req.body[i].nombreTicket,
                precioTicket:req.body[i].precioTicket,
                Pdf: req.body[i].Pdf,
                Total:req.body[i].Total,
                FechaCompra: req.body[i].FechaCompra,
                FechaEvento:req.body[i].FechaEvento,
                NombreEvento:req.body[i].NombreEvento
            };
            console.log(datosCompra)
            mc.query("INSERT INTO compras SET ?", datosCompra, function(error, result){
                if(error){
                    errors = error;
                }

            });
        }
    }
    if(errors){
        res.status(500).json({"Mensaje":"Error"});
        console.log(errors);
    }
    else{
        res.status(201).json({"Mensaje":"Insertado"});
    }

});

app.delete('/evento/:id', function(req, res){
    let id = req.params.id;
    if (mc) {
        console.log(id);
        mc.query("DELETE FROM eventos WHERE id_evento = ?", id, function(error, result){
            if (error) {
                return res.status(500).json({"Mensaje":"Error"});
            }
            else{
                return res.status(200).json({"Mensaje":"Registro con id="+id+" borrado"});
            }
        });
    }
});
app.put('/evento/:id', (req,res)=>{
    let id = req.params.id;
    let evento = req.body;
    console.log(id);
    console.log(evento);
    if (!id || !evento) {
        return res.status(400).send({error: evento, mensaje: 'Debe proveer un id y los datos de un evento'});

    }
    mc.query("UPDATE eventos SET ? WHERE id_evento = ?", [evento, id], function(error, results, fields){
        if (error) throw error;
        return res.status(200).json({"Mensaje":"Registro con id="+id+" ha sido actualizado"});    
        
    });
});
app.get('/eventos/:id', function(req, res) {
    const eventoId = req.params.id; // Obtener el ID del evento de los parámetros de la URL
  
    // Realizar la consulta para obtener el evento por su ID
    mc.query('SELECT * FROM eventos WHERE id_evento = ?', [eventoId], function(error, results, fields) {
      if (error) throw error;
  
      if (results.length === 0) {
        return res.send({
          error: true,
          mensaje: 'Evento no encontrado.'
        });
      }
  
      return res.send({
        error: false,
        data: results[0], // Se asume que el ID es único, por lo tanto se devuelve el primer resultado
        mensaje: 'Detalles del evento.'
      });
    });
  });

app.get('/tickets/:id', function(req, res) {
    const eventoId = req.params.id; // Obtener el ID del evento de los parámetros de la URL
    mc.query('SELECT * FROM `tickets` WHERE evento_id = ?',[eventoId],function(error, results, fields){
        if (error) throw error; 
        return res.send({
            error: false,
            data: results,
            mensaje: 'Lista de tickets'
        });
    });
  });


app.get('/', (req, res, next) =>{
    res.status(200).json({
        ok: true,
        mensaje: 'Peticion realizada correctamente'
    })
});

/* API DE MERCADOPAGO */



app.listen(3000, ()=>{
    console.log('Express Server - puerto 3000 online')
});